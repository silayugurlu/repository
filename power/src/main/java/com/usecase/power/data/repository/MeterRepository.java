package com.usecase.power.data.repository;

import java.util.List;

import org.springframework.validation.annotation.Validated;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionInput;
import com.usecase.power.data.model.FractionList;
import com.usecase.power.data.model.InputStatus;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingInput;
import com.usecase.power.data.model.MeterReadingList;

/**
 *
 * @author silay.ugurlu
 */
public interface MeterRepository {

    /**
     * fetches all fractions from data layer
     *
     * @return list of fractions
     */
    Iterable<Fraction> getAllFractions();

    /**
     * fetches all meter readings from data layer
     *
     * @return list of meter readings
     */
    Iterable<MeterReading> getAllMeterReadings();

    /**
     * inserts list of fractions to data layer
     *
     * @param fractions list
     */
    void createFractions(FractionList fractionList);

    /**
     * inserts list of meter readings to data layer
     *
     * @param meterReadingList
     *
     */
    void createMeterReadings(@Validated MeterReadingList meterReadingList);

    /**
     * finds meter reading value by connectionid, profile and month from data
     * layer
     *
     * @param connectionId
     * @param profile
     * @param month
     * @return meter reading value
     */
    Double findReadingByConnectionIdAndMonth(long connectionId, String profile, int month);

    /**
     * find fractions by profile
     *
     * @param profile
     * @return fraction list
     */
    List<Fraction> findFractionsByProfile(String profile);

    /**
     * inserts fraction input data to data layer
     *
     * @param fractionInputs
     */
    void importFractionInputs(List<FractionInput> fractionInputs);

    /**
     * finds list of profiles bu status from data layer
     *
     * @param fractionStatus
     * @return profile list
     */
    List<String> groupByProfileFractionInput(InputStatus fractionStatus);

    /**
     * find input fraction data by status and profile from data layer
     *
     * @param fractionStatus
     * @param profile
     * @return input fraction list
     */
    List<FractionInput> findFractionInput(InputStatus fractionStatus, String profile);

    /**
     * deletes all data from data layer
     */
    void deleteAll();

    /**
     * deletes all meter readings from data layer
     */
    void deleteMeterReadings();

    /**
     * deletes all fractions from data layer
     */
    void deleteFractions();

    /**
     * deletes fraction input by profile from data layer
     *
     * @param profile
     */
    void deleteFractionInput(String profile);

    /**
     * deletes all fraction input data
     */
    void deleteAllFractionInput();
    
    
    /**
     * deletes all reading input data
     */
    void deleteAllMeterReadingInput();

    /**
     * updates status of fraction by profile in data layer
     *
     * @param fractionStatus
     * @param profile
     */
    void updateFractionInputStatusByProfile(InputStatus fractionStatus, String profile);

    /**
     * inserts input data to data layer
     * 
     * @param meterReadingInputs 
     */
    void importMeterReadingInputs(List<MeterReadingInput> meterReadingInputs);

    /**
     * fetches profile in input data from data layer
     * 
     * @param meterReadingStatus
     * @return list of profile
     */
    List<String> groupByProfileMeterReadingInput(InputStatus meterReadingStatus);

    /**
     * fetches input reading data grouped by profile from data layer
     * 
     * @param meterReadingStatus
     * @param profile
     * @return 
     */
    List<MeterReadingInput> findMeterReadingInput(InputStatus meterReadingStatus, String profile);

    /**
     * deletes input data from data layer
     * 
     * @param profile 
     */
    void deleteMeterReadingInput(String profile);

    /**
     * updates input data status in the data layer
     * 
     * @param meterReadingStatus
     * @param profile 
     */
    void updateMeterReadingInputStatusByProfile(InputStatus meterReadingStatus, String profile);
}
