package com.usecase.power.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author silay.ugurlu
 */
@Entity
public class FractionInput {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String profile;

    private Integer monthOfYear;

    private Double fractionValue;

    private InputStatus status;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMonthOfYear() {
        return monthOfYear;
    }

    public void setMonthOfYear(Integer monthOfYear) {
        this.monthOfYear = monthOfYear;
    }

    public Double getFractionValue() {
        return fractionValue;
    }

    public void setFractionValue(Double fractionValue) {
        this.fractionValue = fractionValue;
    }

    /**
     * @return the status
     */
    public InputStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(InputStatus status) {
        this.status = status;
    }

}
