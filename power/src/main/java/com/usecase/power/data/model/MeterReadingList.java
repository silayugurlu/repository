package com.usecase.power.data.model;

import java.util.List;

/**
 *
 * @author silay.ugurlu
 */
public class MeterReadingList {

    private List<MeterReading> meterReadingList;

    public MeterReadingList() {
        super();
    }

    public MeterReadingList(List<MeterReading> meterReadingList) {
        super();
        this.meterReadingList = meterReadingList;
    }

    /**
     * @return the meterReadingList
     */
    public List<MeterReading> getMeterReadingList() {
        return meterReadingList;
    }

    /**
     * @param meterReadingList the meterReadingList to set
     */
    public void setMeterReadingList(List<MeterReading> meterReadingList) {
        this.meterReadingList = meterReadingList;
    }

}
