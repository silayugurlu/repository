package com.usecase.power.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.usecase.power.data.model.FractionInput;
import com.usecase.power.data.model.InputStatus;

/**
 *
 * @author silay.ugurlu
 */
public interface FractionInputRepository extends CrudRepository<FractionInput, Long> {

    @Query("SELECT fr.profile FROM FractionInput fr WHERE fr.status = :status group by fr.profile")
    List<String> groupByProfile(@Param("status") InputStatus fractionStatus);

    @Query("SELECT fr FROM FractionInput fr WHERE fr.status = :status and  fr.profile = :profile order by fr.monthOfYear")
    List<FractionInput> findFractionInput(@Param("status") InputStatus fractionStatus, @Param("profile") String profile);

    @Transactional
    @Modifying
    @Query("DELETE FractionInput fr WHERE fr.profile = :profile")
    void deleteByProfile(@Param("profile") String profile);

    @Transactional
    @Modifying
    @Query("UPDATE FractionInput fr SET fr.status = :status WHERE fr.profile = :profile")
    void updateStatusByProfile(@Param("status") InputStatus fractionStatus, @Param("profile") String profile);

}
