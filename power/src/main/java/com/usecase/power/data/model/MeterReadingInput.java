package com.usecase.power.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author silay.ugurlu
 */
@Entity
public class MeterReadingInput {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long connectionId;

    private String profile;

    private Integer monthOfYear;

    private Double readingValue;

    private InputStatus status;

    public MeterReadingInput() {
        super();
    }

    public MeterReadingInput(Long connectionId, String profile, Integer monthOfYear, Double readingValue) {
        this.connectionId = connectionId;
        this.profile = profile;
        this.monthOfYear = monthOfYear;
        this.readingValue = readingValue;
    }

    public MeterReadingInput(Long id, Long connectionId, String profile, Integer monthOfYear, Double readingValue) {
        this.id = id;
        this.connectionId = connectionId;
        this.profile = profile;
        this.monthOfYear = monthOfYear;
        this.readingValue = readingValue;
    }

    public Long getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Long connectionId) {
        this.connectionId = connectionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Integer getMonthOfYear() {
        return monthOfYear;
    }

    public void setMonthOfYear(Integer monthOfYear) {
        this.monthOfYear = monthOfYear;
    }

    public Double getReadingValue() {
        return readingValue;
    }

    public void setReadingValue(Double readingValue) {
        this.readingValue = readingValue;
    }

    /**
     * @return the status
     */
    public InputStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(InputStatus status) {
        this.status = status;
    }

}
