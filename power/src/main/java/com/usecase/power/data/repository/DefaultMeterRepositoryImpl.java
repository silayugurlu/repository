package com.usecase.power.data.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionInput;
import com.usecase.power.data.model.FractionList;
import com.usecase.power.data.model.InputStatus;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingInput;
import com.usecase.power.data.model.MeterReadingList;
import com.usecase.power.validation.ValidInput;

/**
 *
 * @author silay.ugurlu
 */
@Repository
public class DefaultMeterRepositoryImpl implements MeterRepository {

    @Autowired
    FractionRepository fractionRepository;

    @Autowired
    FractionInputRepository fractionInputRepository;

    @Autowired
    MeterReadingInputRepository meterReadingInputRepository;

    @Autowired
    MeterReadingRepository meterReadingRepository;

    @Override
    public Iterable<Fraction> getAllFractions() {
        return fractionRepository.findAll();
    }

    @Override
    public Iterable<MeterReading> getAllMeterReadings() {
        return meterReadingRepository.findAll();
    }

    @Override
    public Double findReadingByConnectionIdAndMonth(long connectionId, String profile, int month) {
        return meterReadingRepository.findByConnectionIdAndMonth(connectionId, profile, month);
    }

    @Override
    @ValidInput
    public void createFractions(@Validated FractionList fractions) {
        fractionRepository.save(fractions.getFractionList());
    }

    @Override
    public void importFractionInputs(List<FractionInput> fractionInputs) {
        fractionInputRepository.save(fractionInputs);
    }

    @Override
    public List<String> groupByProfileFractionInput(InputStatus fractionStatus) {
        return fractionInputRepository.groupByProfile(fractionStatus);
    }

    @Override
    public List<FractionInput> findFractionInput(InputStatus fractionStatus, String profile) {
        return fractionInputRepository.findFractionInput(fractionStatus, profile);
    }

    @Override
    public void deleteFractionInput(String profile) {
        fractionInputRepository.deleteByProfile(profile);
    }

    @Override
    public void updateFractionInputStatusByProfile(InputStatus fractionStatus, String profile) {
        fractionInputRepository.updateStatusByProfile(fractionStatus, profile);
    }

    @Override
    public void importMeterReadingInputs(List<MeterReadingInput> meterReadingInputs) {
        meterReadingInputRepository.save(meterReadingInputs);
    }

    @Override
    public List<String> groupByProfileMeterReadingInput(InputStatus fractionStatus) {
        return meterReadingInputRepository.groupByProfile(fractionStatus);
    }

    @Override
    public List<MeterReadingInput> findMeterReadingInput(InputStatus fractionStatus, String profile) {
        return meterReadingInputRepository.findMeterReadingInput(fractionStatus, profile);
    }

    @Override
    public void deleteMeterReadingInput(String profile) {
        meterReadingInputRepository.deleteByProfile(profile);
    }

    @Override
    public void updateMeterReadingInputStatusByProfile(InputStatus fractionStatus, String profile) {
        meterReadingInputRepository.updateStatusByProfile(fractionStatus, profile);
    }

    @Override
    @ValidInput
    public void createMeterReadings(@Validated MeterReadingList meterReadingList) {
        meterReadingRepository.save(meterReadingList.getMeterReadingList());

    }

    @Override
    public void deleteAll() {
        meterReadingRepository.deleteAll();
        fractionRepository.deleteAll();
    }

    @Override
    public void deleteMeterReadings() {
        meterReadingRepository.deleteAll();
    }

    @Override
    public void deleteFractions() {
        fractionRepository.deleteAll();
    }

    @Override
    public List<Fraction> findFractionsByProfile(String profile) {
        return fractionRepository.findByProfile(profile);
    }

    @Override
    public void deleteAllFractionInput() {
        fractionInputRepository.deleteAll();

    }

    @Override
    public void deleteAllMeterReadingInput() {
        meterReadingInputRepository.deleteAll();

    }

}
