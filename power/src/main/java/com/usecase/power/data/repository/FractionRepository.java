package com.usecase.power.data.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.usecase.power.data.model.Fraction;

/**
 *
 * @author silay.ugurlu
 */
public interface FractionRepository extends CrudRepository<Fraction, Long> {

    List<Fraction> findByProfile(String profile);
}
