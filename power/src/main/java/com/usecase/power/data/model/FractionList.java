package com.usecase.power.data.model;

import java.util.List;

/**
 *
 * @author silay.ugurlu
 */
public class FractionList {

    private List<Fraction> fractionList;

    public FractionList() {
        super();
    }

    public FractionList(List<Fraction> fractionList) {
        super();
        this.fractionList = fractionList;
    }

    /**
     * @return the fractionList
     */
    public List<Fraction> getFractionList() {
        return fractionList;
    }

    /**
     * @param fractionList the fractionList to set
     */
    public void setFractionList(List<Fraction> fractionList) {
        this.fractionList = fractionList;
    }

}
