package com.usecase.power.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author silay.ugurlu
 */
@Entity
public class Fraction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String profile;

    private Integer monthOfYear;

    private Double fractionValue;

    public Fraction() {
        super();
    }

    public Fraction(String profile, Integer monthOfYear, Double fractionValue) {
        this.profile = profile;
        this.monthOfYear = monthOfYear;
        this.fractionValue = fractionValue;
    }

    public Fraction(Long id, String profile, Integer monthOfYear, Double fractionValue) {
        super();
        this.id = id;
        this.profile = profile;
        this.monthOfYear = monthOfYear;
        this.fractionValue = fractionValue;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMonthOfYear() {
        return monthOfYear;
    }

    public void setMonthOfYear(Integer monthOfYear) {
        this.monthOfYear = monthOfYear;
    }

    public Double getFractionValue() {
        return fractionValue;
    }

    public void setFractionValue(Double fractionValue) {
        this.fractionValue = fractionValue;
    }

}
