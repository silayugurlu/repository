package com.usecase.power.data.model;

/**
 *
 * @author silay.ugurlu
 */
public enum InputStatus {

    INCOMING, INVALID, VALID

}
