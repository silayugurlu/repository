package com.usecase.power.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.usecase.power.data.model.InputStatus;
import com.usecase.power.data.model.MeterReadingInput;

/**
 *
 * @author silay.ugurlu
 */
public interface MeterReadingInputRepository extends CrudRepository<MeterReadingInput, Long> {

    @Query("SELECT mri.profile FROM MeterReadingInput mri WHERE mri.status = :status group by mri.profile")
    List<String> groupByProfile(@Param("status") InputStatus fractionStatus);

    @Query("SELECT mri FROM MeterReadingInput mri WHERE mri.status = :status and  mri.profile = :profile order by mri.monthOfYear")
    List<MeterReadingInput> findMeterReadingInput(@Param("status") InputStatus fractionStatus, @Param("profile") String profile);

    @Transactional
    @Modifying
    @Query("DELETE MeterReadingInput mri WHERE mri.profile = :profile")
    void deleteByProfile(@Param("profile") String profile);

    @Transactional
    @Modifying
    @Query("UPDATE MeterReadingInput mri SET mri.status = :status WHERE mri.profile = :profile")
    void updateStatusByProfile(@Param("status") InputStatus fractionStatus, @Param("profile") String profile);

}
