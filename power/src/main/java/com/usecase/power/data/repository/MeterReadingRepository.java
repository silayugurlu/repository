package com.usecase.power.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.usecase.power.data.model.MeterReading;

/**
 *
 * @author silay.ugurlu
 */
public interface MeterReadingRepository extends CrudRepository<MeterReading, Long> {

    @Query("SELECT mr.readingValue FROM MeterReading mr WHERE mr.connectionId = :connectionId AND mr.profile = :profile AND mr.monthOfYear = :month")
    Double findByConnectionIdAndMonth(@Param("connectionId") long connectionId, @Param("profile") String profile, @Param("month") int month);

}
