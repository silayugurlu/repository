package com.usecase.power.validation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Validator;

/**
 *
 * @author silay.ugurlu
 */
public class ValidatorRegistry {

    private List<Validator> validatorList = new ArrayList<>();

    public void addValidator(Validator validator) {
        validatorList.add(validator);
    }

    public List<Validator> getValidatorsForObject(Object o) {
        List<Validator> result = new ArrayList<>();
        for (Validator validator : validatorList) {
            if (validator.supports(o.getClass())) {
                result.add(validator);
            }
        }
        return result;
    }
}
