package com.usecase.power.validation;

import java.lang.annotation.Annotation;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;

import com.usecase.power.validator.exception.MeterValidatorException;

/**
 *
 * @author silay.ugurlu
 */
@Aspect
@Component
public class ValidatingAspect {

    @Autowired
    private ValidatorRegistry registry;

    @Before(value = "execution(public * *(..)) && @annotation(com.usecase.power.validation.ValidInput)")
    public void doBefore(JoinPoint point) {
        Annotation[][] paramAnnotations
                = ((MethodSignature) point.getSignature()).getMethod().getParameterAnnotations();
        for (int i = 0; i < paramAnnotations.length; i++) {
            for (Annotation annotation : paramAnnotations[i]) {
                //checking for standard org.springframework.validation.annotation.Validated
                if (annotation.annotationType() == Validated.class) {
                    Object arg = point.getArgs()[i];
                    if (arg == null) {
                        continue;
                    }
                    validate(arg);
                }
            }
        }
    }

    private void validate(Object arg) {
        List<Validator> validatorList = registry.getValidatorsForObject(arg);
        for (Validator validator : validatorList) {
            BindingResult errors = new BeanPropertyBindingResult(arg, arg.getClass().getSimpleName());
            validator.validate(arg, errors);
            if (errors.hasErrors()) {
                throw new MeterValidatorException(errors);
            }
        }
    }
}
