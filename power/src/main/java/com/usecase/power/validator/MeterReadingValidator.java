package com.usecase.power.validator;

import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingList;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author silay.ugurlu
 */
@Component
public class MeterReadingValidator implements Validator {

    private static String errorKey = "invalid.meter.reading";

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == MeterReadingList.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        MeterReadingList list = (MeterReadingList) target;
        List<MeterReading> meterReadings = list.getMeterReadingList();
        Double previousValue = new Double(0);
        for (MeterReading meterReading : meterReadings) {
            Double value = meterReading.getReadingValue();
            if (value < previousValue) {
                if (messageSource != null) {
                    errors.reject(errorKey, messageSource.getMessage(errorKey, null, errorKey, Locale.getDefault()));
                } else {
                    errors.reject(errorKey);
                }
                return;
            }
            previousValue = value;
        }

    }
}
