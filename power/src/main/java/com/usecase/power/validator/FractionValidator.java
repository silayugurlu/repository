package com.usecase.power.validator;

import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionList;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author silay.ugurlu
 */
@Component
public class FractionValidator implements Validator {

    private static String errorKey = "fraction.sum.invalid";

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == FractionList.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        FractionList fractionList = (FractionList) target;
        List<Fraction> fractions = fractionList.getFractionList();
        Double sum = 0d;
        for (Fraction fraction : fractions) {
            sum += fraction.getFractionValue();
        }
        if (sum != 1) {
            if (messageSource != null) {
                errors.reject(errorKey, messageSource.getMessage(errorKey, null, errorKey, Locale.getDefault()));
            } else {
                errors.reject(errorKey);
            }
        }

    }

}
