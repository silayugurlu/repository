package com.usecase.power.validator;

import com.usecase.power.data.model.Fraction;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingList;
import com.usecase.power.data.repository.MeterRepository;
import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author silay.ugurlu
 */
@Component
public class ConsumptionValidator implements Validator {

    private static String errorKey = "inconsistent.consumption";

    @Autowired
    private MeterRepository meterRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public boolean supports(Class<?> clazz) {

        return clazz == MeterReadingList.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        MeterReadingList list = (MeterReadingList) target;
        List<MeterReading> meterReadings = list.getMeterReadingList();

        MeterReading firstReading = meterReadings.get(0);
        MeterReading lastReading = meterReadings.get(meterReadings.size() - 1);

        Double totalConsumption = lastReading.getReadingValue() - firstReading.getReadingValue();

        String profile = firstReading.getProfile();
        List<Fraction> fractions = (List<Fraction>) meterRepository.findFractionsByProfile(profile);

        Double previousValue = new Double(0);

        int index = 0;
        for (MeterReading meterReading : meterReadings) {

            Double value = meterReading.getReadingValue();
            Double consumption = value - previousValue;

            Double fraction = fractions.get(index).getFractionValue();
            Double expectedConsumption = fraction * totalConsumption;

            if (consumption > expectedConsumption + expectedConsumption * 0.25
                    || consumption < expectedConsumption - expectedConsumption * 0.25) {
                if (messageSource != null) {
                    errors.reject(errorKey, messageSource.getMessage(errorKey, null, errorKey, Locale.getDefault()));
                } else {
                    errors.reject(errorKey);
                }
                return;
            }
            previousValue = value;
            index++;
        }

    }

    public void setMeterRepository(MeterRepository meterRepository) {
        this.meterRepository = meterRepository;
    }

}
