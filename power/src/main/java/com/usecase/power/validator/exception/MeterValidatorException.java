package com.usecase.power.validator.exception;

import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 *
 * @author silay.ugurlu
 */
public class MeterValidatorException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -5321080032500012956L;
    private BindingResult bindingResult;

    public MeterValidatorException(BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

    public BindingResult getBindingResult() {
        return bindingResult;
    }

    public String getErrorMessage() {
        StringBuffer errorBuffer = new StringBuffer();
        for (ObjectError error : getBindingResult().getAllErrors()) {           
            errorBuffer.append(error.getDefaultMessage());
            errorBuffer.append(" ");
        }
        return errorBuffer.toString();
    }
}
