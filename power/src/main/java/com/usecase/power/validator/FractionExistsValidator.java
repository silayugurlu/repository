package com.usecase.power.validator;

import com.usecase.power.data.model.Fraction;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingList;
import com.usecase.power.data.repository.MeterRepository;
import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author silay.ugurlu
 */
@Component
public class FractionExistsValidator implements Validator {

    private static String errorKey = "fraction.empty";

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MeterRepository meterRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == MeterReadingList.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        MeterReadingList list = (MeterReadingList) target;
        List<MeterReading> meterReadings = list.getMeterReadingList();
        MeterReading firstReading = meterReadings.get(0);

        String profile = firstReading.getProfile();
        List<Fraction> fractions = (List<Fraction>) meterRepository.findFractionsByProfile(profile);

        if (fractions == null || fractions.isEmpty()) {

            if(messageSource != null){
                errors.reject(errorKey, messageSource.getMessage(errorKey, null, errorKey, Locale.getDefault()));
            }else{
                errors.reject(errorKey);
            }
            

        }

    }

    public void setMeterRepository(MeterRepository meterRepository) {
        this.meterRepository = meterRepository;
    }

}
