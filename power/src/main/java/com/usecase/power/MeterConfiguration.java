/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usecase.power;

import com.usecase.power.validation.ValidatorRegistry;
import com.usecase.power.validator.ConsumptionValidator;
import com.usecase.power.validator.FractionExistsValidator;
import com.usecase.power.validator.FractionValidator;
import com.usecase.power.validator.MeterReadingValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author silay.ugurlu
 */
@Configuration
public class MeterConfiguration {

    @Autowired
    FractionExistsValidator fractionExistsValidator;
    @Autowired
    FractionValidator fractionValidator;

    @Autowired
    MeterReadingValidator meterReadingValidator;
    @Autowired
    ConsumptionValidator consumptionValidator;



    @Bean
    public ValidatorRegistry validatorRegistry() {
        ValidatorRegistry registry = new ValidatorRegistry();

        registry.addValidator(fractionExistsValidator);

        registry.addValidator(fractionValidator);
        registry.addValidator(meterReadingValidator);
        registry.addValidator(consumptionValidator);

        return registry;
    }

}
