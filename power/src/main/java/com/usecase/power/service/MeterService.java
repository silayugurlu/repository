package com.usecase.power.service;

import java.util.List;

import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.FractionOutput;
import com.usecase.power.rest.dto.MeterReadingDto;
import com.usecase.power.rest.dto.MeterReadingOutput;

/**
 *
 * @author silay.ugurlu
 */
public interface MeterService {

    /**
     * gets all fractions
     *
     * @return fraction list
     */
    List<FractionDto> getAllFractions();

    /**
     * gets all meter readings
     *
     * @return meter reading list
     */
    List<MeterReadingDto> getAllMeterReadings();

    /**
     * get meter reading value by connectionId, profile and month
     *
     * @param connectionId
     * @param profile
     * @param month
     * @return meter reading value
     */
    Double findMeterReadingByMonth(long connectionId, String profile, int month);

  /**
   * import fractions and return status
   * 
   * @param fractions
   * @return list of insertion status
   */
    List<FractionOutput> importFractions(List<FractionDto> fractions);


    /**
     * imports meter readings
     * 
     * @param meterReadings
     * @return list of insertion status
     */
    List<MeterReadingOutput> importMeterReadings(List<MeterReadingDto> meterReadings);

    /**
     * calculates consumption for given month of the connection and profile
     *
     * @param connectionId
     * @param profile
     * @param month
     * @return consumption value
     */
    Double getConsumption(long connectionId, String profile, int month);

    /**
     * gets profile
     *
     * @param profile
     * @return fraction list
     */
    List<FractionDto> getProfile(String profile);

    /**
     * deletes all data
     */
    void deleteAll();

    /**
     * deletes all meter readings
     */
    void deleteMeterReadings();

    /**
     * deletes all fractions
     */
    void deleteFractions();

}
