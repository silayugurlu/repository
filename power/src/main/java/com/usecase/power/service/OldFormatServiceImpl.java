package com.usecase.power.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.FractionOutput;
import com.usecase.power.rest.dto.MeterReadingDto;
import com.usecase.power.rest.dto.MeterReadingOutput;

/**
 *
 * @author silay.ugurlu
 */
@Service
public class OldFormatServiceImpl implements OldFormatService {

    @Autowired
    MeterService meterService;

    private static final String LOG_FILE_METER_READING = "LOG_FILE_METER_READING.txt";

    private static final String LOG_FILE_FRACTION = "LOG_FILE_FRACTION.txt";

    private List<FractionDto> fractions;

    private List<MeterReadingDto> meterReadings;

    public void importFractions(String filePath) {
        fractions = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(this::loadFraction);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<FractionOutput> output = meterService.importFractions(fractions);

        writeLogFractions(Paths.get(filePath).getParent().toString(), output);

    }

    public void importMeterReadings(String filePath) {
        meterReadings = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(this::loadFractionLine);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<MeterReadingOutput> output = meterService.importMeterReadings(meterReadings);
        writeLogMeterReading(Paths.get(filePath).getParent().toString(), output);
    }

    private void loadFraction(String line) {
        if (line.length() > 0) {
            String[] parameter = line.split(",");
            try {
                Date date = new SimpleDateFormat("MMM").parse(parameter[0]);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                FractionDto dto = new FractionDto();
                dto.setMonth(c.get(Calendar.MONTH) + 1);
                dto.setProfile(parameter[1]);
                dto.setValue(new Double(parameter[2]));
                fractions.add(dto);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeLogMeterReading(String filePath, List<MeterReadingOutput> output) {
        Path path = Paths.get(filePath + "/" + LOG_FILE_METER_READING);

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            PrintWriter printWriter = new PrintWriter(writer);
            for (MeterReadingOutput mro : output) {
                printWriter.print(mro.getProfile());
                printWriter.print(" : ");
                printWriter.print(mro.getStatus());
                printWriter.println();

            }

        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    private void writeLogFractions(String filePath, List<FractionOutput> output) {

        Path path = Paths.get(filePath + "/" + LOG_FILE_FRACTION);

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            PrintWriter printWriter = new PrintWriter(writer);
            for (FractionOutput fo : output) {
                printWriter.print(fo.getProfile());
                printWriter.print(" : ");
                printWriter.print(fo.getStatus());
                printWriter.println();

            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void loadFractionLine(String line) {
        if (line.length() > 0) {
            String[] parameter = line.split(",");
            try {
                Date date = new SimpleDateFormat("MMM").parse(parameter[2]);
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                MeterReadingDto dto = new MeterReadingDto();
                dto.setConnectionId(new Long(parameter[0]));
                dto.setMonth(c.get(Calendar.MONTH) + 1);
                dto.setProfile(parameter[1]);
                dto.setValue(new Double(parameter[3]));
                meterReadings.add(dto);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
