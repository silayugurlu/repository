package com.usecase.power.service.util;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionInput;
import com.usecase.power.data.model.InputStatus;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingInput;
import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.MeterReadingDto;

/**
 *
 * @author silay.ugurlu
 */
@Component
public class ConverterUtilImpl implements ConverterUtil {

    public MeterReadingDto convertToMeterReadingDTO(MeterReading meterReading) {
        MeterReadingDto dto = new MeterReadingDto();

        dto.setConnectionId(meterReading.getConnectionId());
        dto.setMonth(meterReading.getMonthOfYear());
        dto.setProfile(meterReading.getProfile());
        dto.setValue(meterReading.getReadingValue());
        return dto;
    }

    public List<MeterReadingDto> convertToMeterReadingDtos(List<MeterReading> models) {
        return models.stream().map(this::convertToMeterReadingDTO).collect(Collectors.toList());
    }

    public MeterReading convertToMeterReading(MeterReadingDto meterReadingDto) {
        MeterReading meterReading = new MeterReading();

        meterReading.setConnectionId(meterReadingDto.getConnectionId());
        meterReading.setMonthOfYear(meterReadingDto.getMonth());
        meterReading.setProfile(meterReadingDto.getProfile());
        meterReading.setReadingValue(meterReadingDto.getValue());
        return meterReading;
    }

    public MeterReadingInput convertToMeterReadingInput(MeterReadingDto meterReadingDto) {
        MeterReadingInput meterReadingInput = new MeterReadingInput();

        meterReadingInput.setConnectionId(meterReadingDto.getConnectionId());
        meterReadingInput.setMonthOfYear(meterReadingDto.getMonth());
        meterReadingInput.setProfile(meterReadingDto.getProfile());
        meterReadingInput.setReadingValue(meterReadingDto.getValue());
        meterReadingInput.setStatus(InputStatus.INCOMING);
        return meterReadingInput;
    }

    public List<MeterReadingInput> convertToMeterReadingInputList(List<MeterReadingDto> dtoModels) {
        return dtoModels.stream().map(this::convertToMeterReadingInput).collect(Collectors.toList());
    }

    public FractionDto convertToFractionDto(Fraction fraction) {
        FractionDto dto = new FractionDto();

        dto.setMonth(fraction.getMonthOfYear());
        dto.setProfile(fraction.getProfile());
        dto.setValue(fraction.getFractionValue());
        return dto;
    }

    public List<FractionDto> convertToFractionDtos(List<Fraction> models) {
        return models.stream().map(this::convertToFractionDto).collect(Collectors.toList());
    }

    public FractionInput convertToFraction(FractionDto fractionDto) {
        FractionInput fractionInput = new FractionInput();

        fractionInput.setMonthOfYear(fractionDto.getMonth());
        fractionInput.setFractionValue(fractionDto.getValue());
        fractionInput.setProfile(fractionDto.getProfile());
        fractionInput.setStatus(InputStatus.INCOMING);
        return fractionInput;
    }

    public Fraction convertToFraction(FractionInput fractionInput) {
        Fraction fraction = new Fraction();

        fraction.setMonthOfYear(fractionInput.getMonthOfYear());
        fraction.setFractionValue(fractionInput.getFractionValue());
        fraction.setProfile(fractionInput.getProfile());
        return fraction;
    }

    public List<Fraction> convertToFractionInputList(List<FractionInput> fractionInputs) {
        return fractionInputs.stream().map(this::convertToFraction).collect(Collectors.toList());
    }

    public List<FractionInput> convertToFractionList(List<FractionDto> dtoModels) {
        return dtoModels.stream().map(this::convertToFraction).collect(Collectors.toList());
    }

    public List<MeterReading> convertToMeterReadingList(List<MeterReadingInput> meterReadingInputs) {
        return meterReadingInputs.stream().map(this::convertToMeterReading).collect(Collectors.toList());
    }

    public MeterReading convertToMeterReading(MeterReadingInput meterReadingInput) {
        MeterReading meterReading = new MeterReading();

        meterReading.setConnectionId(meterReadingInput.getConnectionId());
        meterReading.setMonthOfYear(meterReadingInput.getMonthOfYear());
        meterReading.setProfile(meterReadingInput.getProfile());
        meterReading.setReadingValue(meterReadingInput.getReadingValue());
        return meterReading;
    }
}
