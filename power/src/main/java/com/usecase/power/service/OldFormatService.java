package com.usecase.power.service;

/**
 *
 * @author silay.ugurlu
 */
public interface OldFormatService {

    /**
     * reads fraction data from the file and inserts to the data layer
     * 
     * @param filePath 
     */
    void importFractions(String filePath);

    /**
     * reads meterreadings from the file and inserts to the data layer
     * 
     * @param filePath 
     */
    void importMeterReadings(String filePath);

}
