package com.usecase.power.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionInput;
import com.usecase.power.data.model.FractionList;
import com.usecase.power.data.model.InputStatus;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingInput;
import com.usecase.power.data.model.MeterReadingList;
import com.usecase.power.data.repository.MeterRepository;
import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.FractionOutput;
import com.usecase.power.rest.dto.MeterReadingDto;
import com.usecase.power.rest.dto.MeterReadingOutput;
import com.usecase.power.service.util.ConverterUtil;
import com.usecase.power.validator.exception.MeterValidatorException;

/**
 *
 * @author silay.ugurlu
 */
@Service
public class MeterServiceImpl implements MeterService {

    @Autowired
    MeterRepository repository;

    @Autowired
    ConverterUtil converter;

    @Override
    public List<FractionDto> getAllFractions() {
        return converter.convertToFractionDtos((List<Fraction>) repository.getAllFractions());
    }

    @Override
    public List<MeterReadingDto> getAllMeterReadings() {
        return converter.convertToMeterReadingDtos((List<MeterReading>) repository.getAllMeterReadings());
    }

    @Override
    public List<FractionOutput> importFractions(List<FractionDto> fractions) {
        List<FractionOutput> fractionOutputs = new ArrayList<FractionOutput>();
        repository.deleteAllFractionInput();
        repository.importFractionInputs(converter.convertToFractionList(fractions));
        List<String> profiles = repository.groupByProfileFractionInput(InputStatus.INCOMING);
        List<String> profilesToDel = new ArrayList<String>();
        for (String profile : profiles) {
            List<FractionInput> fractionInputs = repository.findFractionInput(InputStatus.INCOMING, profile);
            FractionList fractionList = new FractionList(converter.convertToFractionInputList(fractionInputs));
            try {
                repository.createFractions(fractionList);
                profilesToDel.add(profile);
                fractionOutputs.add(new FractionOutput(profile, "OK"));
            } catch (MeterValidatorException e) {
                repository.updateFractionInputStatusByProfile(InputStatus.INVALID, profile);
                fractionOutputs.add(new FractionOutput(profile, e.getErrorMessage()));
            }
        }
        for (String profile : profilesToDel) {
            repository.deleteFractionInput(profile);
        }
        return fractionOutputs;
    }
    

    @Override
    public List<MeterReadingOutput> importMeterReadings(List<MeterReadingDto> meterReadings) {
        List<MeterReadingOutput> meterReadingOutputs = new ArrayList<MeterReadingOutput>();
        repository.deleteAllMeterReadingInput();
        repository.importMeterReadingInputs(converter.convertToMeterReadingInputList(meterReadings));

        List<String> profiles = repository.groupByProfileMeterReadingInput(InputStatus.INCOMING);
        List<String> profilesToDel = new ArrayList<String>();
        for (String profile : profiles) {
            List<MeterReadingInput> meterReadingInputs = repository.findMeterReadingInput(InputStatus.INCOMING,
                    profile);
            MeterReadingList meterReadingList = new MeterReadingList(
                    converter.convertToMeterReadingList(meterReadingInputs));
            try {
                repository.createMeterReadings(meterReadingList);
                profilesToDel.add(profile);
                meterReadingOutputs.add(new MeterReadingOutput(profile, "OK"));
            } catch (MeterValidatorException e) {
                repository.updateMeterReadingInputStatusByProfile(InputStatus.INVALID, profile);
                meterReadingOutputs.add(new MeterReadingOutput(profile, e.getErrorMessage()));
            }
        }

        for (String profile : profilesToDel) {
            repository.deleteMeterReadingInput(profile);
        }
        return meterReadingOutputs;
    }

    @Override
    public Double findMeterReadingByMonth(long connectionId, String profile, int month) {
        return repository.findReadingByConnectionIdAndMonth(connectionId, profile, month);
    }

    @Override
    public Double getConsumption(long connectionId, String profile, int month) {
        Double thisMonthValue = repository.findReadingByConnectionIdAndMonth(connectionId, profile, month);
        if (thisMonthValue != null) {
            Double previousMonthValue = null;
            int previousMonth = month - 1;

            if (previousMonth > 0) {
                previousMonthValue = repository.findReadingByConnectionIdAndMonth(connectionId, profile, previousMonth);
            }

            return (previousMonthValue != null) ? (thisMonthValue - previousMonthValue) : thisMonthValue;
        }

        return null;
    }

    @Override
    public List<FractionDto> getProfile(String profile) {
        return converter.convertToFractionDtos(repository.findFractionsByProfile(profile));
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteMeterReadings() {
        repository.deleteMeterReadings();
    }

    @Override
    public void deleteFractions() {
        repository.deleteFractions();
    }

    /**
     * for using in test
     *
     * @param repository
     */
    public MeterServiceImpl(MeterRepository repository) {
        this.repository = repository;
    }

}
