package com.usecase.power.service.util;

import java.util.List;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionInput;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingInput;
import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.MeterReadingDto;

/**
 *
 * @author silay.ugurlu
 */
public interface ConverterUtil {

    MeterReadingDto convertToMeterReadingDTO(MeterReading meterReading);

    List<MeterReadingDto> convertToMeterReadingDtos(List<MeterReading> models);

    MeterReading convertToMeterReading(MeterReadingDto meterReadingDto);

    MeterReadingInput convertToMeterReadingInput(MeterReadingDto meterReadingDto);

    List<MeterReadingInput> convertToMeterReadingInputList(List<MeterReadingDto> dtoModels);

    FractionDto convertToFractionDto(Fraction fraction);

    List<FractionDto> convertToFractionDtos(List<Fraction> models);

    FractionInput convertToFraction(FractionDto fractionDto);

    Fraction convertToFraction(FractionInput fractionInput);

    List<Fraction> convertToFractionInputList(List<FractionInput> fractionInputs);

    List<FractionInput> convertToFractionList(List<FractionDto> dtoModels);

    List<MeterReading> convertToMeterReadingList(List<MeterReadingInput> meterReadingInputs);

    MeterReading convertToMeterReading(MeterReadingInput meterReadingInput);
}
