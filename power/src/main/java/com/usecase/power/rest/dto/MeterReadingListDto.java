package com.usecase.power.rest.dto;

import java.util.List;

/**
 *
 * @author silay.ugurlu
 */
public class MeterReadingListDto {

    private List<MeterReadingDto> meterReadingList;

    public MeterReadingListDto(List<MeterReadingDto> meterReadingList) {
        super();
        this.meterReadingList = meterReadingList;
    }

    /**
     * @return the meterReadingList
     */
    public List<MeterReadingDto> getMeterReadingList() {
        return meterReadingList;
    }

    /**
     * @param meterReadingList the meterReadingList to set
     */
    public void setMeterReadingList(List<MeterReadingDto> meterReadingList) {
        this.meterReadingList = meterReadingList;
    }

}
