/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usecase.power.rest.dto;

/**
 *
 * @author silay.ugurlu
 */
public class FractionOutput {
    
    private String profile;
    
    private String status;

    public FractionOutput(String profile, String status) {
        this.profile = profile;
        this.status = status;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
