package com.usecase.power.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usecase.power.service.OldFormatService;

/**
 *
 * @author silay.ugurlu
 */
@RestController
@RequestMapping("/oldFormat")
public class OldFormatController {

    @Autowired
    private OldFormatService oldFormatService;

    @RequestMapping(method = RequestMethod.GET, path = "/test")
    public String test() {
        return "/oldFormat/test";
    }

    @RequestMapping(method = RequestMethod.POST, path = "/fractions")
    public void importFractions(@RequestBody String filePath) {
        oldFormatService.importFractions(filePath);

    }

    @RequestMapping(method = RequestMethod.POST, path = "/readings")
    public void importMeterReadings(@RequestBody String filePath) {
        oldFormatService.importMeterReadings(filePath);
    }

}
