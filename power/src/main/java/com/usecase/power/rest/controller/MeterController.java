package com.usecase.power.rest.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.FractionOutput;
import com.usecase.power.rest.dto.MeterReadingDto;
import com.usecase.power.rest.dto.MeterReadingOutput;
import com.usecase.power.service.MeterService;

/**
 *
 * @author silay.ugurlu
 */
@RestController
@RequestMapping("/meter")
public class MeterController {

    @Autowired
    private MeterService meterService;

    @RequestMapping(method = RequestMethod.GET, path = "/test")
    public String test() {
        return "/meter/test";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/readings")
    public ResponseEntity<List<MeterReadingDto>> getAllReadings() {
        return new ResponseEntity<>((List<MeterReadingDto>) meterService.getAllMeterReadings(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/fractions")
    public ResponseEntity<List<FractionDto>> getAllFractions() {
        return new ResponseEntity<>((List<FractionDto>) meterService.getAllFractions(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/fractions")
    public ResponseEntity<List<FractionOutput>> importFractions(@RequestBody List<FractionDto> fractionsRequestEntity) {
        return new ResponseEntity<>(meterService.importFractions(fractionsRequestEntity), HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.POST, path = "/readings")
    public ResponseEntity<List<MeterReadingOutput>> importMeterReadings(@RequestBody List<MeterReadingDto> meterRequestEntity) {
        return new ResponseEntity<>(meterService.importMeterReadings(meterRequestEntity), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, path = "/consumption/{connectionId}/{profile}/{month}")
    public Double getConsumption(@PathVariable long connectionId, @PathVariable String profile, @PathVariable int month) {
        return meterService.getConsumption(connectionId, profile, month);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/profile/{profile}")
    public ResponseEntity<Collection<FractionDto>> getProfile(@PathVariable String profile) {
        return new ResponseEntity<>((List<FractionDto>) meterService.getProfile(profile), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/readings")
    public void deleteReadings() {
        meterService.deleteMeterReadings();
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/fractions")
    public void deleteFractions() {
        meterService.deleteFractions();
    }

    /**
     * for using in test
     *
     * @param meterService
     */
    public MeterController(MeterService meterService) {
        this.meterService = meterService;
    }

}
