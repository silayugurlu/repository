package com.usecase.power.rest.dto;

/**
 *
 * @author silay.ugurlu
 */
public class MeterReadingDto {

    private Long connectionId;

    private String profile;

    private Integer month;

    private Double value;

    public MeterReadingDto() {
        super();
    }

    public MeterReadingDto(Long connectionId, String profile, Integer month, Double value) {
        super();
        this.connectionId = connectionId;
        this.profile = profile;
        this.month = month;
        this.value = value;
    }

    /**
     * @return the connectionId
     */
    public Long getConnectionId() {
        return connectionId;
    }

    /**
     * @param connectionId the connectionId to set
     */
    public void setConnectionId(Long connectionId) {
        this.connectionId = connectionId;
    }

    /**
     * @return the profile
     */
    public String getProfile() {
        return profile;
    }

    /**
     * @param profile the profile to set
     */
    public void setProfile(String profile) {
        this.profile = profile;
    }

    /**
     * @return the month
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /**
     * @return the value
     */
    public Double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Double value) {
        this.value = value;
    }

}
