package com.usecase.power.rest.dto;

import java.util.List;

/**
 *
 * @author silay.ugurlu
 */
public class FractionListDto {

    private List<FractionDto> fractionList;

    public FractionListDto(List<FractionDto> fractionList) {
        super();
        this.fractionList = fractionList;
    }

    /**
     * @return the fractionList
     */
    public List<FractionDto> getFractionList() {
        return fractionList;
    }

    /**
     * @param fractionList the fractionList to set
     */
    public void setFractionList(List<FractionDto> fractionList) {
        this.fractionList = fractionList;
    }

}
