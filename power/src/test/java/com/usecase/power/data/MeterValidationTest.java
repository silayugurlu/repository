/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usecase.power.data;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.FractionList;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.model.MeterReadingList;
import com.usecase.power.data.repository.MeterRepository;
import com.usecase.power.validator.ConsumptionValidator;
import com.usecase.power.validator.FractionExistsValidator;
import com.usecase.power.validator.FractionValidator;
import com.usecase.power.validator.MeterReadingValidator;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.eq;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

/**
 *
 * @author silay.ugurlu
 */
@RunWith(SpringRunner.class)
public class MeterValidationTest {

    @MockBean
    private MeterRepository repositoryMock;

    private FractionExistsValidator fractionExistsValidator;
    private FractionValidator fractionValidator;
    private MeterReadingValidator meterReadingValidator;
    private ConsumptionValidator consumptionValidator;

    @Before
    public void setUp() {
        fractionValidator = new FractionValidator();
        meterReadingValidator = new MeterReadingValidator();
        fractionExistsValidator = new FractionExistsValidator();
        consumptionValidator = new ConsumptionValidator();

        fractionExistsValidator.setMeterRepository(repositoryMock);
        consumptionValidator.setMeterRepository(repositoryMock);

    }

    @Test
    public void testFractionSum() {
        List<Fraction> fractions = new ArrayList<Fraction>();
        for (int i = 1; i <= 11; i++) {
            Fraction fraction = new Fraction("a", i, 0.05);
            fractions.add(fraction);
        }

        Fraction fraction = new Fraction("a", 12, 0.45);
        fractions.add(fraction);
        FractionList fractionList = new FractionList(fractions);
        Errors errors = Mockito.mock(Errors.class);

        fractionValidator.validate(fractionList, errors);
        verify(errors, times(0)).reject("fraction.sum.invalid");

    }

    @Test
    public void testFractionSumException() {
        List<Fraction> fractions = new ArrayList<Fraction>();
        for (int i = 1; i <= 12; i++) {
            Fraction fraction = new Fraction("a", i, new Double(i) / 12 - 0.1);
            fractions.add(fraction);
        }
        FractionList fractionList = new FractionList(fractions);
        Errors errors = Mockito.mock(Errors.class);

        fractionValidator.validate(fractionList, errors);

        verify(errors, times(1)).reject("fraction.sum.invalid");
    }

    @Test
    public void testIncreasingMeterReading() {

        List<MeterReading> meterReadings = new ArrayList<MeterReading>();
        for (int i = 1; i <= 12; i++) {
            MeterReading meterReading = new MeterReading(1L, "a", i, new Double(i));
            meterReadings.add(meterReading);
        }
        MeterReadingList meterReadingList = new MeterReadingList(meterReadings);

        Errors errors = Mockito.mock(Errors.class);
        meterReadingValidator.validate(meterReadingList, errors);
        verify(errors, times(0)).reject("invalid.meter.reading");
    }

    @Test
    public void testIncreasingMeterReadingException() {

        List<MeterReading> meterReadings = new ArrayList<MeterReading>();
        for (int i = 1; i <= 11; i++) {
            MeterReading meterReading = new MeterReading(1L, "a", i, new Double(i));
            meterReadings.add(meterReading);
        }

        MeterReading meterReading = new MeterReading(1L, "a", 12, 9.0);
        meterReadings.add(meterReading);
        MeterReadingList meterReadingList = new MeterReadingList(meterReadings);
        Errors errors = Mockito.mock(Errors.class);
        meterReadingValidator.validate(meterReadingList, errors);
        verify(errors, times(1)).reject("invalid.meter.reading");
    }

    @Test
    public void testFractionExists() {

        List<MeterReading> meterReadings = new ArrayList<MeterReading>();
        for (int i = 1; i <= 12; i++) {
            MeterReading meterReading = new MeterReading(1L, "a", i, new Double(i));
            meterReadings.add(meterReading);
        }
        MeterReadingList meterReadingList = new MeterReadingList(meterReadings);

        List<Fraction> fractions = new ArrayList<Fraction>();

        Fraction fraction = new Fraction("a", 1, 0.05);
        fractions.add(fraction);

        when(repositoryMock.findFractionsByProfile(eq("a"))).thenReturn(fractions);
        Errors errors = Mockito.mock(Errors.class);
        fractionExistsValidator.validate(meterReadingList, errors);
        verify(errors, times(0)).reject("fraction.empty");
    }

    @Test
    public void testFractionExistsException() {

        List<MeterReading> meterReadings = new ArrayList<MeterReading>();
        for (int i = 1; i <= 12; i++) {
            MeterReading meterReading = new MeterReading(1L, "a", i, new Double(i));
            meterReadings.add(meterReading);
        }
        MeterReadingList meterReadingList = new MeterReadingList(meterReadings);

        List<Fraction> fractions = new ArrayList<Fraction>();

        Fraction fraction = new Fraction("a", 1, 0.05);
        fractions.add(fraction);

        when(repositoryMock.findFractionsByProfile(eq("a"))).thenReturn(null);
        Errors errors = Mockito.mock(Errors.class);
        fractionExistsValidator.validate(meterReadingList, errors);
        verify(errors, times(1)).reject("fraction.empty");
    }

    @Test
    public void testConsistentConsumption() {
        List<MeterReading> meterReadings = new ArrayList<MeterReading>();

        MeterReading meterReading1 = new MeterReading(1L, "a", 1, new Double(18.0));
        meterReadings.add(meterReading1);
        MeterReading meterReading2 = new MeterReading(1L, "a", 2, new Double(73.0));
        meterReadings.add(meterReading2);
        MeterReading meterReading3 = new MeterReading(1L, "a", 3, new Double(258.0));
        meterReadings.add(meterReading3);
        MeterReadingList meterReadingList = new MeterReadingList(meterReadings);

        List<Fraction> fractions = new ArrayList<Fraction>();
        Fraction fraction1 = new Fraction("a", 1, 0.1);
        fractions.add(fraction1);
        Fraction fraction2 = new Fraction("a", 2, 0.2);
        fractions.add(fraction2);
        Fraction fraction3 = new Fraction("a", 3, 0.7);
        fractions.add(fraction3);

        when(repositoryMock.findFractionsByProfile(eq("a"))).thenReturn(fractions);
        Errors errors = Mockito.mock(Errors.class);
        consumptionValidator.validate(meterReadingList, errors);
        verify(errors, times(0)).reject("inconsistent.consumption");
    }

    @Test
    public void testConsistentConsumptionException() {
        List<MeterReading> meterReadings = new ArrayList<MeterReading>();

        MeterReading meterReading1 = new MeterReading(1L, "a", 1, new Double(18.0));
        meterReadings.add(meterReading1);
        MeterReading meterReading2 = new MeterReading(1L, "a", 2, new Double(80.0));
        meterReadings.add(meterReading2);
        MeterReading meterReading3 = new MeterReading(1L, "a", 3, new Double(250.0));
        meterReadings.add(meterReading3);
        MeterReadingList meterReadingList = new MeterReadingList(meterReadings);

        List<Fraction> fractions = new ArrayList<Fraction>();
        Fraction fraction1 = new Fraction("a", 1, 0.1);
        fractions.add(fraction1);
        Fraction fraction2 = new Fraction("a", 2, 0.2);
        fractions.add(fraction2);
        Fraction fraction3 = new Fraction("a", 3, 0.7);
        fractions.add(fraction3);

        when(repositoryMock.findFractionsByProfile(eq("a"))).thenReturn(fractions);
        Errors errors = Mockito.mock(Errors.class);
        consumptionValidator.validate(meterReadingList, errors);
        verify(errors, times(1)).reject("inconsistent.consumption");
    }
}
