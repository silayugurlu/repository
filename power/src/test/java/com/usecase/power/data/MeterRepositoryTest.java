/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usecase.power.data;

import com.usecase.power.data.model.Fraction;
import com.usecase.power.data.model.MeterReading;
import com.usecase.power.data.repository.MeterRepository;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

/**
 *
 * @author silay.ugurlu
 */
@RunWith(SpringRunner.class)
@ComponentScan("com.usecase.power.data.repository")
@DataJpaTest
public class MeterRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MeterRepository meterRepository;

    @Test
    public void testFindReadingByConnectionIdAndMonth() {
        entityManager.persist(new MeterReading(1L, "a", 1, 15.0));
        Double reading = meterRepository.findReadingByConnectionIdAndMonth(1L, "a", 1);
        assertEquals(15.0, reading);
    }

    @Test
    public void testFindFractionsByProfile() {
        entityManager.persist(new Fraction("a", 1, 0.1));
        entityManager.persist(new Fraction("a", 2, 0.2));
        entityManager.persist(new Fraction("b", 1, 0.3));
        List<Fraction> fractions = meterRepository.findFractionsByProfile("a");
        assertEquals(2, fractions.size());
    }

}
