/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usecase.power.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.usecase.power.rest.controller.MeterController;
import com.usecase.power.rest.dto.FractionDto;
import com.usecase.power.rest.dto.MeterReadingDto;
import com.usecase.power.service.MeterService;
import java.util.List;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.hasSize;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author silay.ugurlu
 */
@RunWith(SpringRunner.class)
@ComponentScan("com.usecase.power")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class MeterApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    MeterService meterService;

    @Autowired
    ObjectMapper objectMapper;

    private MeterController meterController;

    @Before
    public void setUp() {
        meterController = new MeterController(meterService);
    }

    @Test
    public void testFractions() throws Exception {
        List<FractionDto> fractions = new ArrayList<FractionDto>();
        fractions.add(new FractionDto("a", 1, 0.2));
        fractions.add(new FractionDto("a", 2, 0.2));
        given(meterService.getAllFractions()).willReturn(fractions);

        mockMvc.perform(get("/meter/fractions/")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    public void testMeterReadings() throws Exception {
        List<MeterReadingDto> meterReadings = new ArrayList<MeterReadingDto>();
        meterReadings.add(new MeterReadingDto(1L, "a", 1, 5.0));
        meterReadings.add(new MeterReadingDto(1L, "a", 2, 6.0));
        given(meterService.getAllMeterReadings()).willReturn(meterReadings);

        mockMvc.perform(get("/meter/readings/")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    public void testConsumption() throws Exception {
        given(meterService.getConsumption(1L, "a", 2)).willReturn(1.0);

        mockMvc.perform(get("/meter/consumption/1/a/2")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", is(1.0)));

    }

}
