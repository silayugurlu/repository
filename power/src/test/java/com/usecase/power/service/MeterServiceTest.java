/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usecase.power.service;

import com.usecase.power.data.repository.MeterRepository;
import static junit.framework.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author silay.ugurlu
 */
@RunWith(SpringRunner.class)
public class MeterServiceTest {

    @MockBean
    private MeterRepository repositoryMock;

    private MeterService service;

    @Before
    public void setUp() {
        service = new MeterServiceImpl(repositoryMock);
    }
    
    @Test
    public void testConsumption(){
         when(repositoryMock.findReadingByConnectionIdAndMonth(eq(1L),eq("a"),eq(1))).thenReturn(10.0);
         when(repositoryMock.findReadingByConnectionIdAndMonth(eq(1L),eq("a"),eq(2))).thenReturn(17.0);
         
         Double consumption = service.getConsumption(1L, "a", 2);
         assertEquals(7.0, consumption);
    }

}
