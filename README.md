#  README # 

## Energy Consumption REST API ##

Application contains following services:

* GET /meter/consumption/{connectionId}/{profile}/{month} getConsumption
* DELETE /meter/fractions deleteFractions
* GET /meter/fractions getAllFractions
* POST /meter/fractions importFractions
* GET /meter/profile/{profile} getProfile
* DELETE /meter/readings deleteReadings
* GET /meter/readings getAllReadings
* POST /meter/readings importMeterReadings

For old format file input:

* POST /oldFormat/fractions importFractions
* POST /oldFormat/readings importMeterReadings

Documentation file is under documentation folder.
H2 in memory database is used and project is built with maven, runs on 8080 port.

### To run the application type: ###
```sh

	$ ./mvn spring-boot:run
```
